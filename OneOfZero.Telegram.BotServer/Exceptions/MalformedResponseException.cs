﻿using System;
using System.Runtime.Serialization;

namespace OneOfZero.Telegram.BotServer.Exceptions
{
	public class MalformedResponseException : Exception
	{
		public MalformedResponseException()
		{
		}

		public MalformedResponseException(string message) 
			: base(message)
		{
		}

		public MalformedResponseException(string message, Exception innerException)
			: base(message, innerException)
		{
		}

		protected MalformedResponseException(SerializationInfo info, StreamingContext context)
			: base(info, context)
		{
		}
	}
}