﻿using Newtonsoft.Json;

namespace OneOfZero.Telegram.BotServer.Entities.Inbound
{
	public class Update : IIdentifiableByInteger
	{
		[JsonProperty("update_id")]
		public int Id { get; set; }

		public Message Message { get; set; }
	}
}