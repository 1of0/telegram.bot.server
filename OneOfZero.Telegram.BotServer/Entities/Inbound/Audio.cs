﻿using Newtonsoft.Json;

namespace OneOfZero.Telegram.BotServer.Entities.Inbound
{
	public class Audio : AbstractFile
	{
		public int Duration { get; set; }
	}
}