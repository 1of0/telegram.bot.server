﻿namespace OneOfZero.Telegram.BotServer.Entities.Inbound
{
	public class Video : AbstractFile
	{
		public int Width { get; set; }

		public int Height { get; set; }

		public int Duration { get; set; }

		public PhotoSize Thumb { get; set; }

		public string Caption { get; set; }
	}
}