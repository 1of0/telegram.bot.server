﻿namespace OneOfZero.Telegram.BotServer.Entities.Inbound
{
	public class Document : AbstractFile
	{
		public PhotoSize Thumb { get; set; }

		public string FileName { get; set; }
	}
}