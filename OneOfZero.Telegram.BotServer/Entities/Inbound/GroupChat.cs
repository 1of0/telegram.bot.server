﻿namespace OneOfZero.Telegram.BotServer.Entities.Inbound
{
	public class Groupchat : BaseConversation
	{
		public string Title { get; set; }

		public override bool Equals(object obj)
		{
			if (obj == null || !(obj is User))
			{
				return false;
			}

			return ((User)obj).Id == Id;
		}
	}
}