﻿namespace OneOfZero.Telegram.BotServer.Entities.Inbound
{
	public class Location
	{
		public float Longitude { get; set; }

		public float Latitude { get; set; }
	}
}