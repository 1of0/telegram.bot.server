﻿using Newtonsoft.Json;

namespace OneOfZero.Telegram.BotServer.Entities.Inbound
{
	public class Sticker : PhotoSize
	{
		public PhotoSize Thumb { get; set; }
	}
}