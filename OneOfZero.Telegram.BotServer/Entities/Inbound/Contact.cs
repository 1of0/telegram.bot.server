﻿namespace OneOfZero.Telegram.BotServer.Entities.Inbound
{
	public class Contact
	{
		public string PhoneNumber { get; set; }

		public string FirstName { get; set; }

		public string LastName { get; set; }

		public string UserId { get; set; }
	}
}