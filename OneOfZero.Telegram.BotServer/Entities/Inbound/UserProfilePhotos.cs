﻿using System.Collections.Generic;

namespace OneOfZero.Telegram.BotServer.Entities.Inbound
{
	public class UserProfilePhotos
	{
		public List<PhotoSize> Photos { get; set; }

		public int TotalCount { get; set; }
	}
}