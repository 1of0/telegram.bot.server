﻿using Newtonsoft.Json;

namespace OneOfZero.Telegram.BotServer.Entities.Inbound
{
	public class ApiResponse<T>
	{
		[JsonProperty("ok")]
		public bool Success { get; set; }

		public T Result { get; set; }
	}
}