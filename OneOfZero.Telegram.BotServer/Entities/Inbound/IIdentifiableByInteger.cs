﻿namespace OneOfZero.Telegram.BotServer.Entities.Inbound
{
	public interface IIdentifiableByInteger
	{
		int Id { get; set; }
	}
}
