﻿using Newtonsoft.Json;

namespace OneOfZero.Telegram.BotServer.Entities.Inbound
{
	public class PhotoSize
	{
		[JsonProperty("file_id")]
		public string Id { get; set; }

		public int Width { get; set; }

		public int Height { get; set; }

		public int? FileSize { get; set; }
	}
}