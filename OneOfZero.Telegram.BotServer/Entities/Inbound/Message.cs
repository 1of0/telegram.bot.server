﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using OneOfZero.Telegram.BotServer.Helpers.Json;

namespace OneOfZero.Telegram.BotServer.Entities.Inbound
{
	public class Message : IIdentifiableByInteger
	{
		[JsonProperty("message_id")]
		public int Id { get; set; }

		public User From { get; set; }

		[JsonConverter(typeof(UnixTimestampConverter))]
		public DateTime Date { get; set; }

		[JsonConverter(typeof(ConversationConverter))]
		public BaseConversation Chat { get; set; }

		public User ForwardFrom { get; set; }

		[JsonConverter(typeof(UnixTimestampConverter))]
		public DateTime? ForwardDate { get; set; }

		public Message ReplyToMessage { get; set; }

		public string Text { get; set; }

		public Document Document { get; set; }

		public Audio Audio { get; set; }

		public Video Video { get; set; }

		public List<PhotoSize> Photo { get; set; }

		public Sticker Sticker { get; set; }

		public Contact Contact { get; set; }

		public Location Location { get; set; }

		public User NewChatParticipant { get; set; }

		public User LeftChatParticipant { get; set; }

		public string NewChatTitle { get; set; }

		public List<PhotoSize> NewChatPhoto { get; set; }

		public bool? DeleteChatPhoto { get; set; }

		public bool? GroupChatCreated { get; set; }
	}
}