﻿using Newtonsoft.Json;

namespace OneOfZero.Telegram.BotServer.Entities.Inbound
{
	public abstract class AbstractFile
	{
		[JsonProperty("file_id")]
		public string Id { get; set; }

		public string MimeType { get; set; }

		public int? FileSize { get; set; }
	}
}
