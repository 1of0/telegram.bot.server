﻿namespace OneOfZero.Telegram.BotServer.Entities.Inbound
{
	public class User : BaseConversation
	{
		public string FirstName { get; set; }

		public string LastName { get; set; }

		public string Username { get; set; }

		public override bool Equals(object obj)
		{
			if (obj == null || !(obj is User))
			{
				return false;
			}

			return ((User)obj).Id == Id;
		}
	}
}