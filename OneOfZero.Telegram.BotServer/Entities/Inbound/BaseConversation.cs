﻿namespace OneOfZero.Telegram.BotServer.Entities.Inbound
{
	public class BaseConversation : IIdentifiableByInteger
	{
		public int Id { get; set; }

		public BaseConversation()
		{
		}

		public BaseConversation(int id)
		{
			Id = id;
		}
	}
}