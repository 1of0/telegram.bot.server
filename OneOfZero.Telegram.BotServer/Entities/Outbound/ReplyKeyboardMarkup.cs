﻿namespace OneOfZero.Telegram.BotServer.Entities.Outbound
{
	public class ReplyKeyboardMarkup : IReplyMarkup
	{
		public string[,] Keyboard { get; set; }

		public bool? ResizeKeyboard { get; set; }

		public bool? OneTimeKeyboard { get; set; }

		public bool? Selective { get; set; }

		public ReplyKeyboardMarkup()
		{
		}

		public ReplyKeyboardMarkup(string[,] keyboard)
		{
			Keyboard = keyboard;
		}
	}
}
