﻿namespace OneOfZero.Telegram.BotServer.Entities.Outbound
{
	public class FileReference : IFile
	{
		public string FileId { get; set; }
	}
}
