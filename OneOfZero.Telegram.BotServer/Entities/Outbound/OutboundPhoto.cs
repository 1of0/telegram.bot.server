﻿using OneOfZero.Telegram.BotServer.Entities.Inbound;
using OneOfZero.Telegram.BotServer.Helpers.Form;

namespace OneOfZero.Telegram.BotServer.Entities.Outbound
{
	[ApiMethod("sendPhoto")]
	public class OutboundPhoto : AbstractOutboundEntity
	{
		[FormConverter(typeof(FileFormConverter))]
		public IFile Photo { get; set; }

		public string Caption { get; set; }

		public OutboundPhoto()
		{
		}

		public OutboundPhoto(BaseConversation chat, IFile photo)
		{
			Chat = chat;
			Photo = photo;
		}
	}
}