﻿using OneOfZero.Telegram.BotServer.Entities.Inbound;
using OneOfZero.Telegram.BotServer.Helpers.Form;

namespace OneOfZero.Telegram.BotServer.Entities.Outbound
{
	[ApiMethod("sendMessage")]
	public class OutboundMessage : AbstractOutboundEntity
	{
		public string Text { get; set; }

		public bool? DisableWebPagePreview { get; set; }

		public OutboundMessage()
		{
		}

		public OutboundMessage(BaseConversation chat, string text)
		{
			Chat = chat;
			Text = text;
		}
	}
}