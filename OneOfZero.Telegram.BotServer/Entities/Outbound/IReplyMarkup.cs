﻿namespace OneOfZero.Telegram.BotServer.Entities.Outbound
{
	public interface IReplyMarkup
	{
		bool? Selective { get; set; }
	}
}
