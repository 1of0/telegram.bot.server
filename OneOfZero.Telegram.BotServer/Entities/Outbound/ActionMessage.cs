﻿using OneOfZero.Telegram.BotServer.Entities.Inbound;
using OneOfZero.Telegram.BotServer.Helpers.Form;

namespace OneOfZero.Telegram.BotServer.Entities.Outbound
{
	[ApiMethod("sendAction")]
	public class ActionMessage : IOutboundEntity
	{
		[FormProperty("chat_id")]
		[FormConverter(typeof(IdConverter))]
		public BaseConversation Chat { get; set; }

		[FormConverter(typeof(EnumConverter))]
		public Action Action { get; set; }
	}
}