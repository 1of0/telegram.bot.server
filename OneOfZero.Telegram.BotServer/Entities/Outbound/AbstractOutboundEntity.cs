﻿using OneOfZero.Telegram.BotServer.Entities.Inbound;
using OneOfZero.Telegram.BotServer.Helpers.Form;

namespace OneOfZero.Telegram.BotServer.Entities.Outbound
{
	public class AbstractOutboundEntity : IOutboundEntity
	{
		[FormProperty("chat_id")]
		[FormConverter(typeof(IdConverter))]
		public BaseConversation Chat { get; set; }

		[FormProperty("reply_to_message_id")]
		public Message ReplyTo { get; set; }

		public IReplyMarkup ReplyMarkup { get; set; }
	}
}
