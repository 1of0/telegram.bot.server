﻿using OneOfZero.Telegram.BotServer.Entities.Inbound;
using OneOfZero.Telegram.BotServer.Helpers.Form;

namespace OneOfZero.Telegram.BotServer.Entities.Outbound
{
	[ApiMethod("forwardMessage")]
	public class ForwardedMessage : IOutboundEntity
	{
		[FormProperty("chat_id")]
		[FormConverter(typeof(IdConverter))]
		public BaseConversation Chat { get; set; }

		[FormProperty("from_chat_id")]
		[FormConverter(typeof(IdConverter))]
		public BaseConversation FromChat { get; set; }

		[FormProperty("message_id")]
		[FormConverter(typeof(IdConverter))]
		public Message Message { get; set; }
	}
}