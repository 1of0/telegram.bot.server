﻿namespace OneOfZero.Telegram.BotServer.Entities.Outbound
{
	public enum Action
	{
		Typing,
		UploadPhoto,
		RecordVideo,
		UploadVideo,
		RecordAudio,
		UploadAudio,
		UploadDocument,
		FindLocation
	}
}