﻿namespace OneOfZero.Telegram.BotServer.Entities.Outbound
{
	public class ReplyKeyboardHide : IReplyMarkup
	{
		public bool HideKeyboard { get; set; }

		public bool? Selective { get; set; }

		public ReplyKeyboardHide()
		{
			HideKeyboard = true;
		}
	}
}
