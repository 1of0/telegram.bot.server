﻿using System.IO;

namespace OneOfZero.Telegram.BotServer.Entities.Outbound
{
	public class FileInput : IFile
	{
		public string Filename { get; set; }

		public Stream Stream { get; set; }

		public FileInput()
		{
		}

		public FileInput(string filePath)
		{
			Filename = Path.GetFileName(filePath);
			Stream = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
		}
	}
}
