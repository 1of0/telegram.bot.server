﻿using OneOfZero.Telegram.BotServer.Entities.Inbound;
using OneOfZero.Telegram.BotServer.Helpers.Form;

namespace OneOfZero.Telegram.BotServer.Entities.Outbound
{
	public interface IOutboundEntity
	{
		BaseConversation Chat { get; set; }
	}
}
