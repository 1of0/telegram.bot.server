﻿using System;

namespace OneOfZero.Telegram.BotServer.Entities.Outbound
{
	[AttributeUsage(AttributeTargets.Class)]
	public class ApiMethodAttribute : Attribute
	{
		public string Name { get; set; }

		public ApiMethodAttribute(string name)
		{
			Name = name;
		}
	}
}
