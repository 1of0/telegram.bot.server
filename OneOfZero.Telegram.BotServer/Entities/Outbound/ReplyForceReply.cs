﻿namespace OneOfZero.Telegram.BotServer.Entities.Outbound
{
	public class ReplyForceReply : IReplyMarkup
	{
		public bool ForceReply { get; set; }

		public bool? Selective { get; set; }

		public ReplyForceReply()
		{
			ForceReply = true;
		}
	}
}
