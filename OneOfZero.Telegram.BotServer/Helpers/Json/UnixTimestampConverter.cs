﻿using Newtonsoft.Json;
using System;

namespace OneOfZero.Telegram.BotServer.Helpers.Json
{
	public class UnixTimestampConverter : JsonConverter
	{
		private static readonly DateTime UnixEpoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);

		public override bool CanConvert(Type objectType)
		{
			return objectType == typeof(DateTime) || objectType == typeof(DateTime?);
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			return UnixEpoch.AddSeconds(Int64.Parse(reader.Value.ToString()));
		}

		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			writer.WriteRawValue(((DateTime)value - UnixEpoch).TotalSeconds.ToString());
		}
	}
}