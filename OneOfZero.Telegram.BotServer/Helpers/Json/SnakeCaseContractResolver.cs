﻿using System.Text.Transformation;
using Newtonsoft.Json.Serialization;

namespace OneOfZero.Telegram.BotServer.Helpers.Json
{
	public class SnakeCaseContractResolver : DefaultContractResolver
	{
		protected override string ResolvePropertyName(string propertyName)
		{
			return propertyName.ToSnakeCase();
		}
	}
}
