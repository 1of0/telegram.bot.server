﻿using Newtonsoft.Json;

namespace OneOfZero.Telegram.BotServer.Helpers.Json
{
	public static class Json
	{
		private static readonly JsonSerializerSettings JsonSettings = new JsonSerializerSettings
		{
			ContractResolver = new SnakeCaseContractResolver(),
			NullValueHandling = NullValueHandling.Ignore
		};

		public static string Serialize<T>(T obj)
		{
			return JsonConvert.SerializeObject(obj, JsonSettings);
		}

		public static T Deserialize<T>(string json)
		{
			return JsonConvert.DeserializeObject<T>(json, JsonSettings);
		}
	}
}
