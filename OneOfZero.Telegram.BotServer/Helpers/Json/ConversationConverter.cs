﻿using System;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OneOfZero.Telegram.BotServer.Entities.Inbound;

namespace OneOfZero.Telegram.BotServer.Helpers.Json
{
	public class ConversationConverter : JsonConverter
	{
		public override bool CanConvert(Type objectType)
		{
			return typeof(BaseConversation).IsAssignableFrom(objectType);
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			if (!typeof(BaseConversation).IsAssignableFrom(objectType))
			{
				throw new NotSupportedException();
			}

			JObject jObject = JObject.Load(reader);
			if (jObject.Properties().Any(p => p.Name == "username"))
			{
				return jObject.ToObject<User>();
			}
			else
			{
				return jObject.ToObject<Groupchat>();
			}
		}

		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			serializer.Serialize(writer, value);
		}
	}
}