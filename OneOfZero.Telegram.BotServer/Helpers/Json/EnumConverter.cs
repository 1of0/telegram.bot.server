﻿using System;
using System.Text.Transformation;
using Newtonsoft.Json;

namespace OneOfZero.Telegram.BotServer.Helpers.Json
{
	public class EnumConverter : JsonConverter
	{
		public override bool CanConvert(Type objectType)
		{
			return objectType.IsEnum;
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			if (!objectType.IsEnum)
			{
				throw new NotSupportedException();
			}

			string stringValue = serializer.Deserialize<string>(reader);

			return Enum.Parse(objectType, stringValue.ToPascalCase());
		}

		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			serializer.Serialize(writer, value.ToString().ToSnakeCase());
		}
	}
}