﻿using System;
using Newtonsoft.Json;
using OneOfZero.Telegram.BotServer.Entities.Inbound;

namespace OneOfZero.Telegram.BotServer.Helpers.Json
{
	public class IdConverter : JsonConverter
	{
		public override bool CanConvert(Type objectType)
		{
			return typeof(IIdentifiableByInteger).IsAssignableFrom(objectType);
		}

		public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
		{
			throw new NotSupportedException();
		}

		public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
		{
			if (value == null)
			{
				return;
			}

			serializer.Serialize(writer, ((IIdentifiableByInteger)value).Id);
		}
	}
}