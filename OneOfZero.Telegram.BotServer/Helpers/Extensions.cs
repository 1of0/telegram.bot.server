﻿using System;
using System.Net.Http;
using System.Reflection;
using System.Text.Transformation;
using OneOfZero.Telegram.BotServer.Helpers.Form;

namespace OneOfZero.Telegram.BotServer.Helpers
{
	public static class Extensions
	{
		public static HttpContent AsHttpContent<T>(this T obj)
		{
			if (obj == null)
			{
				return null;
			}

			var formBuilder = new FormBuilder();

			foreach (var property in obj.GetType().GetProperties())
			{
				object value = property.GetValue(obj);

				if (value == null
				|| property.GetCustomAttribute<FormIgnoreAttribute>() != null
				|| !property.GetMethod.Attributes.HasFlag(MethodAttributes.Public))
				{
					continue;
				}

				string fieldName = property.Name.ToSnakeCase();

				var propertyAttribute = property.GetCustomAttribute<FormPropertyAttribute>();
				if (propertyAttribute != null && !String.IsNullOrEmpty(propertyAttribute.Name))
				{
					fieldName = propertyAttribute.Name;
				}

				var converterAttribute = property.GetCustomAttribute<FormConverterAttribute>();
				if (converterAttribute != null)
				{
					var converter = (IFormConverter)Activator.CreateInstance(converterAttribute.ConverterType);
					converter.Write(fieldName, property.GetValue(obj), formBuilder);
					continue;
				}

				if (property.PropertyType.IsPrimitive
				|| property.PropertyType.IsEnum
				|| property.PropertyType == typeof(string))
				{
					formBuilder.AddField(fieldName, property.GetValue(obj).ToString());
					continue;
				}

				formBuilder.AddField(fieldName, Json.Json.Serialize(property.GetValue(obj)));
			}

			return formBuilder.ToHttpContent();
		}
	}
}