﻿using System;

namespace OneOfZero.Telegram.BotServer.Helpers.Form
{
	[AttributeUsage(AttributeTargets.Property)]
	public class FormIgnoreAttribute : Attribute
	{
	}
}
