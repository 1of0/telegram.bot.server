﻿namespace OneOfZero.Telegram.BotServer.Helpers.Form
{
	public interface IFormConverter
	{
		void Write(string fieldName, object value, FormBuilder formBuilder);
	}
}
