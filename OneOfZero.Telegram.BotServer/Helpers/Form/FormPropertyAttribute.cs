﻿using System;

namespace OneOfZero.Telegram.BotServer.Helpers.Form
{
	[AttributeUsage(AttributeTargets.Property)]
	public class FormPropertyAttribute : Attribute
	{
		public string Name { get; set; }

		public FormPropertyAttribute()
		{
		}

		public FormPropertyAttribute(string name)
		{
			Name = name;
		}
	}
}
