﻿using System.Text.Transformation;

namespace OneOfZero.Telegram.BotServer.Helpers.Form
{
	public class EnumConverter : IFormConverter
	{
		public void Write(string fieldName, object value, FormBuilder formBuilder)
		{
			formBuilder.AddField(fieldName, value.ToString().ToSnakeCase());
		}
	}
}