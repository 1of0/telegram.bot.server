﻿using OneOfZero.Telegram.BotServer.Entities.Inbound;

namespace OneOfZero.Telegram.BotServer.Helpers.Form
{
	public class IdConverter : IFormConverter
	{
		public void Write(string fieldName, object value, FormBuilder formBuilder)
		{
			if (value == null)
			{
				return;
			}

			formBuilder.AddField(fieldName, ((IIdentifiableByInteger)value).Id.ToString());
		}
	}
}