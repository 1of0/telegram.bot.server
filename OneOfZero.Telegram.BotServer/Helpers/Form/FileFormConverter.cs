﻿using OneOfZero.Telegram.BotServer.Entities.Outbound;

namespace OneOfZero.Telegram.BotServer.Helpers.Form
{
	public class FileFormConverter : IFormConverter
	{
		public void Write(string fieldName, object value, FormBuilder formBuilder)
		{
			if (!(value is IFile))
			{
				return;
			}

			FileInput stream = value as FileInput;
			if (stream != null)
			{
				formBuilder.AddFileInput(fieldName, stream);
				return;
			}

			FileReference reference = value as FileReference;
			if (reference != null)
			{
				formBuilder.AddField(fieldName, reference.FileId);
			}
		}
	}
}
