﻿using System.Net.Http;
using OneOfZero.Telegram.BotServer.Entities.Outbound;

namespace OneOfZero.Telegram.BotServer.Helpers.Form
{
	public class FormBuilder
	{
		private readonly MultipartFormDataContent _form = new MultipartFormDataContent();

		public void AddField(string fieldName, string value)
		{
			_form.Add(new StringContent(value), fieldName);
		}

		public void AddFileInput(string fieldName, FileInput file)
		{
			_form.Add(new StreamContent(file.Stream), fieldName, file.Filename);
		}

		public HttpContent ToHttpContent()
		{
			return _form;
		}
	}
}
