﻿using System;

namespace OneOfZero.Telegram.BotServer.Helpers.Form
{
	[AttributeUsage(AttributeTargets.Property)]
	public class FormConverterAttribute : Attribute
	{
		private Type _converterType;

		public Type ConverterType
		{
			get { return _converterType; }
			set
			{
				if (value != null && !typeof(IFormConverter).IsAssignableFrom(value))
				{
					throw new ArgumentException("A converter must implement the IFormConverter interface", "value");
				}
				_converterType = value;
			}
		}

		public FormConverterAttribute()
		{
		}

		public FormConverterAttribute(Type converterType)
		{
			ConverterType = converterType;
		}
	}
}
