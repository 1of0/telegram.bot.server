﻿using System;
using System.Reflection;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using OneOfZero.Telegram.BotServer.Common;
using OneOfZero.Telegram.BotServer.Helpers;
using OneOfZero.Telegram.BotServer.Helpers.Json;
using OneOfZero.Telegram.BotServer.Entities.Inbound;
using OneOfZero.Telegram.BotServer.Entities.Outbound;
using OneOfZero.Telegram.BotServer.Exceptions;

namespace OneOfZero.Telegram.BotServer
{
	public class BotInstance
	{
		private const string ApiBase = "https://api.telegram.org";
		private const string UpdateDeltaKey = "LastUpdate";
		private const string MethodGetUpdates = "getUpdates";
		private const string MethodGetMe = "getMe";
		private const int DefaultHttpPollingInterval = 500;

		private readonly HttpClient _client = new HttpClient();

		private readonly object _syncRoot = new object();

		private readonly CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource();

		private bool _isRunning;

		public string ApiToken { get; }

		public IKeyValuePersistenceAdapter Persistence { get; }

		public int HttpPollingInterval { get; }

		public event EventHandler<Update> UpdateReceived;

		public event EventHandler<Exception> ExceptionThrown;

		public BotInstance(string apiToken, IKeyValuePersistenceAdapter persistenceAdapter)
		{
			ApiToken = apiToken;
			Persistence = persistenceAdapter;
			HttpPollingInterval = DefaultHttpPollingInterval;
		}

		public void Start()
		{
			lock (_syncRoot)
			{
				if (_isRunning)
				{
					return;
				}

				_isRunning = true;
			}
			HttpPoller();
		}

		public void Stop()
		{
			lock (_syncRoot)
			{
				_cancellationTokenSource.Cancel();
				_isRunning = false;
			}
		}

		public async Task<ApiResponse<User>> GetMe()
		{
			return await ApiCall<User>(HttpMethod.Get, MethodGetMe);
		}

		public async Task<ApiResponse<Message>> SendAsync(IOutboundEntity entity)
		{
			var apiMethodAttribute = entity.GetType().GetCustomAttribute<ApiMethodAttribute>();

			if (apiMethodAttribute == null)
			{
				throw new ArgumentException(
					"Entities provided to the Send() method must have an [ApiMethod] attribute."
				);
			}

			return await ApiCall<Message>(HttpMethod.Post, apiMethodAttribute.Name, entity.AsHttpContent());
		}

		protected void HttpPoller(Task task = null)
		{
			try
			{
				if (_cancellationTokenSource.IsCancellationRequested)
				{
					return;
				}

				Thread.Sleep(HttpPollingInterval);

				_client
					.GetAsync(BuildUpdateUri(), _cancellationTokenSource.Token)
					.ContinueWith(ProcessResponse, _cancellationTokenSource.Token)
					.ContinueWith(HttpPoller, _cancellationTokenSource.Token)
				;
			}
			catch (Exception e)
			{
				OnExceptionThrown(e);
			}
			finally
			{
				task?.Dispose();
			}
		}

		protected async void ProcessResponse(Task<HttpResponseMessage> responseTask)
		{
			try
			{
				using (var httpResponse = responseTask.Result)
				{
					var apiResponse = await ParseResponse<List<Update>>(httpResponse);

					if (!apiResponse.Success || apiResponse.Result.Count <= 0)
					{
						return;
					}

					foreach (var update in apiResponse.Result)
					{
						try
						{
							OnUpdateReceived(update);
						}
						catch (Exception e)
						{
							OnExceptionThrown(e);
						}
					}

					Persistence.Set(UpdateDeltaKey, apiResponse.Result.Last().Id);
				}
			}
			catch (Exception e)
			{
				OnExceptionThrown(e);
			}
			finally
			{
				responseTask.Dispose();
			}
		}

		protected async Task<ApiResponse<T>> ApiCall<T>(HttpMethod httpMethod, string apiMethod, HttpContent httpData = null)
		{
			string url = BuildMethodUri(apiMethod);
			HttpResponseMessage httpResponse;

			if (httpMethod == HttpMethod.Get)
			{
				httpResponse = await _client.GetAsync(url);
			}
			else if (httpMethod == HttpMethod.Post)
			{
				httpResponse = await _client.PostAsync(url, httpData);
			}
			else if (httpMethod == HttpMethod.Put)
			{
				httpResponse = await _client.PutAsync(url, httpData);
			}
			else if (httpMethod == HttpMethod.Delete)
			{
				httpResponse = await _client.DeleteAsync(url);
			}
			else
			{
				return null;
			}

			return await ParseResponse<T>(httpResponse);
		}

		protected string BuildUpdateUri()
		{
			int updateDelta = Persistence.ContainsKey(UpdateDeltaKey) ? Persistence.Get<int>(UpdateDeltaKey) + 1 : 0;

			return $"{BuildMethodUri(MethodGetUpdates)}?offset={updateDelta}";
		}

		protected string BuildMethodUri(string method)
		{
			return $"{ApiBase}/bot{ApiToken}/{method}";
		}

		protected async Task<ApiResponse<T>> ParseResponse<T>(HttpResponseMessage response)
		{
			using (response)
			{
				response.EnsureSuccessStatusCode();

				using (var stream = response.Content as StreamContent)
				{
					if (stream == null)
					{
						throw new MalformedResponseException("Response could not be decoded as a StreamContent object");
					}

					string json = await stream.ReadAsStringAsync();
					return Json.Deserialize<ApiResponse<T>>(json);
				}
			}
		}

		protected virtual void OnUpdateReceived(Update update)
		{
			UpdateReceived?.Invoke(this, update);
		}

		protected virtual void OnExceptionThrown(Exception e)
		{
			ExceptionThrown?.Invoke(this, e);
		}
	}
}
