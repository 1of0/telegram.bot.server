#!/bin/sh
### BEGIN INIT INFO
# Provides:          kaasbot
# Required-Start:    $network $local_fs
# Required-Stop:
# Should-Start:      $named
# Should-Stop:
# Default-Start:        2 3 4 5
# Default-Stop:         0 1 6
# Short-Description:    kaasbot server
# Description:          kaasbot server

### END INIT INFO

. /lib/lsb/init-functions

PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
NAME=Kaasbot
DAEMON_ROOT=/root/projects/out/kaasbot
DAEMON=$DAEMON_ROOT/KaasBot.exe
DAEMON_OPTS=
USER=root
PIDFILE=$DAEMON_ROOT/KaasBot.pid
LOG=$DAEMON_ROOT/KaasBot.log

log_use_fancy_output 1

case "$1" in

	start)
 		log_begin_msg "Starting daemon: $NAME"
		start-stop-daemon --start --background --quiet --pidfile $PIDFILE --make-pidfile --chdir $DAEMON_ROOT --exec $DAEMON -- $DAEMON_OPTS >> $LOG 2>&1
		log_success_msg
		;;

	stop)
		log_begin_msg "Stopping daemon: $NAME"
		start-stop-daemon --stop --quiet --oknodo --pidfile $PIDFILE
		rm -f $PIDFILE
		log_success_msg
		;;

	restart)
		log_begin_msg "Restarting daemon: $NAME"
		start-stop-daemon --stop --quiet --oknodo --retry 30 --pidfile $PIDFILE
		start-stop-daemon --start --background --quiet --pidfile $PIDFILE --make-pidfile --chdir $DAEMON_ROOT --exec $DAEMON -- $DAEMON_OPTS >> $LOG 2>&1
		log_success_msg
		;;

	status)
		status_of_proc -p $PIDFILE $DAEMON $NAME && exit 0 || exit $?
		;;

	*)
		echo "Usage: "$1" {start|stop|restart|status}"
		exit 1

esac

exit 0

