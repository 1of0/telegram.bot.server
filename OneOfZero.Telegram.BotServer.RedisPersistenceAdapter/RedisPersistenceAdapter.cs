﻿using Newtonsoft.Json;
using StackExchange.Redis;
using OneOfZero.Telegram.BotServer.Common;

namespace OneOfZero.Telegram.BotServer.RedisPersistenceAdapter
{
	public class RedisPersistenceAdapter : IKeyValuePersistenceAdapter
	{
		public IDatabase Database { get; private set; }

		public RedisPersistenceAdapter()
		{
			Database = ConnectionMultiplexer.Connect("localhost").GetDatabase();
		}

		public RedisPersistenceAdapter(IConnectionMultiplexer connectionMultiplexer)
		{
			Database = connectionMultiplexer.GetDatabase();
		}

		public RedisPersistenceAdapter(IDatabase database)
		{
			Database = database;
		}

		public bool Set<T>(string key, T value)
		{
			return Database.StringSet(key, JsonConvert.SerializeObject(value));
		}

		public T Get<T>(string key)
		{
			RedisValue json = Database.StringGet(key);
			return json.IsNull ? default(T) : JsonConvert.DeserializeObject<T>(json);
		}

		public bool ContainsKey(string key)
		{
			return Database.KeyExists(key);
		}

		public ulong Increment(string key)
		{
			return (ulong)Database.StringIncrement(key);
		}

		public ulong Decrement(string key)
		{
			return (ulong)Database.StringDecrement(key);
		}
	}
}
