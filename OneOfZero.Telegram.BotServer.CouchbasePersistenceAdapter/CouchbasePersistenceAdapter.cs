﻿using Couchbase;
using Couchbase.Core;
using OneOfZero.Telegram.BotServer.Common;
using System;

namespace OneOfZero.Telegram.BotServer.CouchbasePersistenceAdapter
{
	public class CouchbasePersistenceAdapter : IKeyValuePersistenceAdapter, IDisposable
	{
		public IBucket Bucket { get; private set; }

		public CouchbasePersistenceAdapter()
		{
			Bucket = new Cluster().OpenBucket();
		}

		public CouchbasePersistenceAdapter(IBucket bucket)
		{
			Bucket = bucket;
		}

		public CouchbasePersistenceAdapter(ICluster cluster, string bucketName = null)
		{
			Bucket = bucketName == null
				? cluster.OpenBucket()
				: cluster.OpenBucket(bucketName)
			;
		}

		public bool Set<T>(string key, T value)
		{
			var result = Bucket.Upsert(new Document<T> { Id = key, Content = value });
			return result.Success;
		}

		public T Get<T>(string key)
		{
			var result = Bucket.Get<T>(key);
			return result.Success ? result.Value : default(T);
		}

		public bool ContainsKey(string key)
		{
			return Bucket.Exists(key);
		}

		public ulong Increment(string key)
		{
			var result = Bucket.Increment(key);
			return result.Success ? result.Value : 0;
		}

		public ulong Decrement(string key)
		{
			var result = Bucket.Decrement(key);
			return result.Success ? result.Value : 0;
		}

		public void Dispose()
		{
			Bucket.Dispose();
		}
	}
}
