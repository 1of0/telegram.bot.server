﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace BtwBot
{
	public class TextDb
	{
		private object _syncRoot = new object();

		private string _filename;

		private Dictionary<string, string> _cache = new Dictionary<string, string>();

		public TextDb(string filename)
		{
			_filename = filename;

			if (File.Exists(filename))
			{
				Load();
				ReplaceState();
			}
		}

		private void Load()
		{
			using (StreamReader reader = File.OpenText(_filename))
			{
				string line;
				while ((line = reader.ReadLine()) != null)
				{
					string[] parts = line.Split('|');
					string key = Encoding.UTF8.GetString(Convert.FromBase64String(parts[0]));
					string value = Encoding.UTF8.GetString(Convert.FromBase64String(parts[1]));
					if (_cache.ContainsKey(key))
					{
						_cache[key] = value;
					}
					else
					{
						_cache.Add(key, value);
					}
				}
			}
		}

		private void ReplaceState()
		{
			try
			{
				File.Delete(_filename);
			}
			catch
			{
			}

			using (FileStream stream = File.OpenWrite(_filename))
			using (StreamWriter writer = new StreamWriter(stream))
			{
				foreach (var entry in _cache)
				{
					writer.WriteLine(String.Format(
						"{0}|{1}",
						Convert.ToBase64String(Encoding.UTF8.GetBytes(entry.Key)),
						Convert.ToBase64String(Encoding.UTF8.GetBytes(entry.Value))
					));
					writer.Flush();
				}
			}
		}

		public string this[string key]
		{
			get
			{
				return _cache.ContainsKey(key) ? _cache[key] : null;
			}
			set
			{
				if (_cache.ContainsKey(key))
				{
					_cache[key] = value;
				}
				else
				{
					_cache.Add(key, value);
				}

				lock (_syncRoot)
				{
					using (StreamWriter writer = File.AppendText(_filename))
					{
						writer.WriteLine(String.Format(
							"{0}|{1}",
							Convert.ToBase64String(Encoding.UTF8.GetBytes(key)),
							Convert.ToBase64String(Encoding.UTF8.GetBytes(value))
						));
						writer.Flush();
					}
				}
			}
		}

		public bool ContainsKey(string key)
		{
			return _cache.ContainsKey(key);
		}

		public T Get<T>(string key, T defaultValue = default(T))
		{
			if (!_cache.ContainsKey(key))
			{
				return defaultValue;
			}

			if (typeof(T) == typeof(int))
			{
				return (T)Convert.ChangeType(Int32.Parse(_cache[key]), typeof(T));
			}

			return (T)Convert.ChangeType(_cache[key], typeof(T));
		}

		public void Set<T>(string key, T value)
		{
			string stringValue = (string)Convert.ChangeType(value, typeof(string));
			this[key] = stringValue;
		}
	}
}