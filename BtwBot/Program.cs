﻿using System;
using System.Collections.Generic;
using System.Text.Transformation;
using System.Threading;
using System.Threading.Tasks;
using OneOfZero.Telegram.BotServer;
using OneOfZero.Telegram.BotServer.Entities.Inbound;
using OneOfZero.Telegram.BotServer.Entities.Outbound;

namespace BtwBot
{
	class Program
	{
		private static Dictionary<BaseConversation, ConverstationContext> _contextIndex =
			new Dictionary<BaseConversation, ConverstationContext>();

		static void Main(string[] args)
		{
			TextDbAdapter persistence = new TextDbAdapter(new TextDb("btwbot.txt"));
			var bot = new BotInstance("86399827:AAHL5vaCKMmjK1SSJXIIVZmpi0MDdJ-aWQ0", persistence);
			bot.UpdateReceived += bot_UpdateReceived;
			bot.ExceptionThrown += bot_ExceptionThrown;

			Task.Factory.StartNew(bot.Start, TaskCreationOptions.LongRunning);

			while (true)
			{
				Thread.Sleep(500);
			}
		}

		static void bot_ExceptionThrown(object sender, Exception e)
		{
			Console.WriteLine("Exception: {0}", e.Message);
		}

		static async void bot_UpdateReceived(object sender, Update update)
		{
			BotInstance instance = (BotInstance)sender;
			BaseConversation conversation = update.Message.Chat;

			ConverstationContext context;
			if (!_contextIndex.ContainsKey(conversation))
			{
				_contextIndex.Add(conversation, new ConverstationContext());
			}
			context = _contextIndex[conversation];

			string message = update.Message.Text;

			if (String.IsNullOrEmpty(message))
			{
				return;
			}

			if (message == "/start" || message == "/reset")
			{
				_contextIndex[conversation] = context = new ConverstationContext();
				await instance.SendAsync(new OutboundMessage(conversation, "Test") { ReplyMarkup = new ReplyKeyboardMarkup(ToKeyboard<Action>()) });
				await instance.SendAsync(new OutboundPhoto(conversation, new FileInput("kaas.png")));
				return;
			}

			try
			{
				switch (context.State)
				{
					case State.ExpectingAction:
						//context.State
						return;

				}
			}
			catch (Exception e)
			{
				Console.WriteLine("Exception in update handler: {0}", e.Message);
			}
		}

		public static string[,] ToKeyboard<T>() where T : struct, IConvertible
		{
			typeof(T).EnsureIsEnum();

			string[] enumNames = Enum.GetNames(typeof(T));
			string[,] keyboard = new string[enumNames.Length, 1];

			for (int i = 0; i < enumNames.Length; i++)
			{
				keyboard[i, 0] = enumNames[i].ToFirstWordCase();
			}

			return keyboard;
		}
	}

	public static class Extensions
	{
		public static T GetEnumValue<T>(this string name) where T : struct, IConvertible
		{
			typeof(T).EnsureIsEnum();

			T value;

			if (!Enum.TryParse(name.ToPascalCase(), out value))
			{
				throw new ArgumentException("The provided name is not a member of the provided enum type", "name");
			}

			return value;
		}

		public static void EnsureIsEnum(this Type type)
		{
			if (type == null)
			{
				throw new ArgumentNullException("type");
			}

			if (!type.IsEnum)
			{
				throw new NotSupportedException("Provided type T is not an enumerable type");
			}
		}
	}

	class ConverstationContext
	{
		public State State { get; set; }
	}

	enum State
	{
		ExpectingAction,
		ExpectingProductName,
		ExpectingProductPrice,
		ExpectingQuantityType,
		ExpectingQuantity,
		ExpectingTaxType,
		ExpectingConfirmation,
		ExpectingListItemSelection
	}

	enum QuantityType
	{
		PerItem,
		PerKilo
	}

	enum TaxType
	{
		Food,
		Other
	}

	enum Action
	{
		AddProduct,
		ModifyProduct,
		RemoveProduct,
		ListProducts,
		ShowTotal
	}
}
