﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using OneOfZero.Telegram.BotServer;
using OneOfZero.Telegram.BotServer.Entities.Inbound;
using OneOfZero.Telegram.BotServer.Entities.Outbound;

namespace KaasBot
{
	using StringDictionary = Dictionary<string, string>;

	internal class Program
	{
		private static readonly StringDictionary KaasTriggers = new StringDictionary
		{
			{ "kaas", "kaas!" },
			{ "cheese", "kaas!" },
			{ "queso", "¿no hablo español?" },
			{ "fromage", "kaas!" },
			{ "käse", "kaas!" },
			{ "keshi", "kaas!" },
			{ "keju", "kaas!" },
			{ "queijo", "kaas!" }
		};

		private static void Main(string[] args)
		{
			string token = File.ReadAllText("token.txt");
			var persistence = new TextDbAdapter(new TextDb("kaasbot.txt"));

			var bot = new BotInstance(token, persistence);
			bot.UpdateReceived += bot_UpdateReceived;
			bot.ExceptionThrown += bot_ExceptionThrown;

			Task.Factory.StartNew(bot.Start, TaskCreationOptions.LongRunning);

			while (true)
			{
				Thread.Sleep(500);
			}
		}

		static void bot_ExceptionThrown(object sender, Exception exception)
		{
			Console.WriteLine("Exception thrown: {0}", exception.Message);
		}

		static async void bot_UpdateReceived(object sender, Update update)
		{
			Console.WriteLine("Received update; ID: {0}", update.Id);

			if (String.IsNullOrEmpty(update.Message.Text))
			{
				return;
			}

			var kaasResponse = KaasTriggers.FirstOrDefault(
				kv => update.Message.Text.ToLower().Contains(kv.Key.ToLower())
			).Value;

			if (String.IsNullOrEmpty(kaasResponse))
			{
				return;
			}

			try
			{
				await ((BotInstance)sender).SendAsync(new OutboundMessage
				{
					Chat = update.Message.Chat,
					Text = kaasResponse
				});
			}
			catch (Exception e)
			{
				Console.WriteLine("Exception thrown in update handler: {0}", e.Message);
			}
		}
	}
}