﻿using OneOfZero.Telegram.BotServer.Common;

namespace KaasBot
{
	public class TextDbAdapter : IKeyValuePersistenceAdapter
	{
		private readonly TextDb _instance;

		public TextDbAdapter(TextDb instance)
		{
			_instance = instance;
		}

		public bool Set<T>(string key, T value)
		{
			_instance.Set(key, value);
			return true;
		}

		public T Get<T>(string key)
		{
			return _instance.Get<T>(key);
		}

		public bool ContainsKey(string key)
		{
			return _instance.ContainsKey(key);
		}

		public ulong Increment(string key)
		{
			int value = _instance.ContainsKey(key) ? _instance.Get<int>(key) + 1 : 1;
			_instance.Set(key, value);
			return (ulong)value;
		}

		public ulong Decrement(string key)
		{
			int value = _instance.ContainsKey(key) ? _instance.Get<int>(key) - 1 : -1;
			_instance.Set(key, value);
			return (ulong)value;
		}
	}
}