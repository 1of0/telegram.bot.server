﻿using System;
using Enyim.Caching;
using Enyim.Caching.Memcached;
using OneOfZero.Telegram.BotServer.Common;

namespace OneOfZero.Telegram.BotServer.MemcachedPersistenceAdapter
{
	public class MemcachedPersistenceAdapter : IKeyValuePersistenceAdapter, IDisposable
	{
		public MemcachedClient Client { get; private set; }

		public MemcachedPersistenceAdapter()
		{
			Client = new MemcachedClient();
		}

		public bool Set<T>(string key, T value)
		{
			return Client.Store(StoreMode.Replace, key, value);
		}

		public T Get<T>(string key)
		{
			return Client.Get<T>(key);
		}

		public bool ContainsKey(string key)
		{
			object value;
			return Client.TryGet(key, out value);
		}

		public ulong Increment(string key)
		{
			return Client.Increment(key, 0, 1);
		}

		public ulong Decrement(string key)
		{
			return Client.Decrement(key, 0, 1);
		}

		public void Dispose()
		{
			Client.Dispose();
		}
	}
}
