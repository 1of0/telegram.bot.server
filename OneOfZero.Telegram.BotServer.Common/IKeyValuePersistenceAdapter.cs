﻿using System.Collections.Generic;

namespace OneOfZero.Telegram.BotServer.Common
{
	/// <summary>
	/// Describes a persistence adapter for a key/value storage system.
	/// </summary>
	public interface IKeyValuePersistenceAdapter
	{
		/// <summary>
		/// Should set the value of the record indicated by the provided <paramref name="key"/> to the provided 
		/// <paramref name="value"/>. Should return a boolean value indicating whether or not the operation was a 
		/// success.
		/// </summary>
		/// <typeparam name="T">Type of the value</typeparam>
		/// <param name="key">Key of the record</param>
		/// <param name="value">Value to store</param>
		/// <returns>Boolean value indicating whether or not the operation was a success.</returns>
		bool Set<T>(string key, T value);

		/// <summary>
		/// Should get the value of the record indicated by the provided <paramref name="key"/>.
		/// </summary>
		/// <typeparam name="T">Expected type of the value</typeparam>
		/// <param name="key">Key of the record</param>
		/// <returns>Fetched value</returns>
		/// <exception cref="KeyNotFoundException">Thrown if there is no record for the given key.</exception>
		T Get<T>(string key);

		/// <summary>
		/// Should return a boolean value indicating whether or not a record exists with the provided 
		/// <paramref name="key"/>.
		/// </summary>
		/// <param name="key">Key to check</param>
		/// <returns>A boolean value indicating whether or not a record exists with the provided <paramref name="key"/>.
		/// </returns>
		bool ContainsKey(string key);

		/// <summary>
		/// Should assume the value of the record for the provided <paramref name="key"/> is numeric, and increment the
		/// value of the record by one. The new value should be returned.
		/// </summary>
		/// <param name="key">Key of the record</param>
		/// <returns>New value</returns>
		ulong Increment(string key);

		/// <summary>
		/// Should assume the value of the record for the provided <paramref name="key"/> is numeric, and decrement the
		/// value of the record by one. The new value should be returned.
		/// </summary>
		/// <param name="key">Key of the record</param>
		/// <returns>New value</returns>
		ulong Decrement(string key);
	}
}
